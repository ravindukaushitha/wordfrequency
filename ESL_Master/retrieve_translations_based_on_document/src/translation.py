import os
from dotenv import load_dotenv
import openai

load_dotenv()
openai.api_key = os.environ.get("API_KEY")


def call_chat_gpt(prompt):
    """
    Calls the GPT-3.5-turbo model to generate a response based on the provided prompt while acting as an ESL assistant.

    Args:
        prompt (str): The prompt to be sent to the model.

    Returns:
        str: The generated response from the model.
    """
    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=[
            {"role": "system", "content": "You are a helpful ESL assistant."},
            {"role": "user", "content": f'{prompt}'},
        ]
    )

    translation = response["choices"][0]["message"]["content"]
    return translation


def single_translate_word(word: str, document: str, target_language: str) -> str:
    """
    Translates a single word into the target language.
    Args:
        word (str): The word to be translated.
        document (str): The context in which the word is used.
        target_language (str): The target language for the translation.
    Returns:
        str: The translated word.
    """

    prompt = f"Give me single word translation of {word} into {target_language}."
    try:
        translation = call_chat_gpt(prompt)
    except openai.OpenAIError:
        translation = "An error occurred while trying to translate the word. Please try again later."

    return translation


def multiple_translate_word(word: str, document: str, target_language: str) -> str:
    """
    Provides all possible translations of a word into the target language.

    Args:
        word (str): The word to be translated.
        document (str): The context in which the word is used.
        target_language (str): The target language for the translation.

    Returns:
        str: All possible translations of the word.
    """

    prompt = f"Give me all possible translations of the word {word} into {target_language}."
    try:
        translation = call_chat_gpt(prompt)
    except openai.OpenAIError:
        translation = "An error occurred while trying to translate the word. Please try again later."

    return translation


def translate_word(word: str, document: str, target_language: str) -> str:
    """
    Translates a word and provides all possible translations with their corresponding contexts and examples.

    Args:
        word (str): The word to be translated.
        document (str): The context in which the word is used.
        target_language (str): The target language for the translation.

    Returns:
        str: The translations with their corresponding contexts and examples.
    """

    prompt = f"As an English language learner, I need to translate the word '{word}' from the context '{document}' into {target_language}. Please provide all possible translations with their corresponding contexts and examples."
    try:
        translation = call_chat_gpt(prompt)
    except openai.OpenAIError:
        translation = "An error occurred while trying to translate the word. Please try again later."

    return translation



