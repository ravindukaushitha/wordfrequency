from flask import Flask, jsonify, request
from flask_cors import CORS
from translation import single_translate_word, multiple_translate_word, translate_word

app = Flask(__name__)
CORS(app)

@app.route('/single_translate', methods=['POST'])
def single_translate():
    """
    Endpoint to handle single word translation requests.

    Expects a JSON object with 'word', 'document', and 'target_language' keys.

    Returns:
        A JSON object containing the target language, the original word, and the translated word.
    """    
    data = request.get_json()
    word = data.get('word')
    document = data.get('document')
    target_language = data.get('target_language')

    translation = single_translate_word(word, document, target_language)

    response = {
        "language": target_language,
        "word": word,
        "translation": translation
    }

    return jsonify(response)

@app.route('/multiple_translate', methods=['POST'])
def multiple_translate():
    """
    Endpoint to handle multiple translations of a single word.

    Expects a JSON object with 'word', 'document', and 'target_language' keys.

    Returns:
        A JSON object containing the target language, the original word, and all possible translations of the word.
    """
    data = request.get_json()
    word = data.get('word')
    document = data.get('document')
    target_language = data.get('target_language')

    translations = multiple_translate_word(word, document, target_language)

    response = {
        "language": target_language,
        "word": word,
        "translations": translations
    }

    return jsonify(response)

@app.route('/translate', methods=['POST'])
def translate():
    """
    Endpoint to handle translation requests with context and examples.

    Expects a JSON object with 'word', 'document', and 'target_language' keys.

    Returns:
        A JSON object containing the target language, the original word, and the translations with their corresponding contexts and examples.
    """
    data = request.get_json()
    word = data.get('word')
    document = data.get('document')
    target_language = data.get('target_language')

    translation = translate_word(word, document, target_language)

    response = {
        "language": target_language,
        "word": word,
        "translation": translation
    }

    return jsonify(response)

if __name__ == '__main__':
    app.run(debug=True)
