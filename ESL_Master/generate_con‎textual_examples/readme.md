This folder is for US [#3](https://gitlab.com/mxm_yrmnk/wordfrequency/-/issues/3) Personalized Contextual Examples, ChatGPT feature.

If you are using ChatGPT's free tier API key, sometimes the program returns error. This is because OpenAI limits the number of API calls on this tier. It can be easily solved by waiting for sometime (2-3 mins) and then calling our endpoint again.

