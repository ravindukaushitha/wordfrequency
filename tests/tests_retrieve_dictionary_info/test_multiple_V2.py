# test_multiple.py

import pytest
from main import get_multiple_definitions


def test_get_dictionary_info_multiple_definitions():
  # Test a word with multiple definitions
  word = "run"
  result = get_multiple_definitions(word, 2)
  assert result is not None
  assert result["word"] == word
  assert len(result["definitions"]) > 1

  # Test a string with multiple definitions
  word = "open-minded"
  result = get_multiple_definitions(word, 2)
  assert result is not None
  assert result["word"] == word
  assert len(result["definitions"]) > 1

  # Test a common word with multiple definitions
  word = "apple"
  result = get_multiple_definitions(word, 2)
  assert result is not None
  assert result["word"] == word
  assert len(result["definitions"]) > 1

  # Test another common word with multiple definitions
  word = "book"
  result = get_multiple_definitions(word, 2)
  assert result is not None
  assert result["word"] == word
  assert len(result["definitions"]) > 1
