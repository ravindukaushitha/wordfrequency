import sys
from pathlib import Path
sys.path.insert(0, str(Path(__file__).parent.parent.parent))

from ESL_Master.retrieve_translations_based_on_document.src.translation import call_chat_gpt, single_translate_word, multiple_translate_word, translate_word
from unittest.mock import MagicMock, patch
import pytest
from openai import OpenAIError
 
 
def test_chatgpt_api_call():
     # Test that the ChatGPT API is called with the correct parameters when requesting a translation.
    prompt = "Translate 'word' into 'en'."
    with patch("ESL_Master.retrieve_translations_based_on_document.src.translation.openai.ChatCompletion.create") as mock_chat_completion_create:
        call_chat_gpt(prompt)
        mock_chat_completion_create.assert_called_once_with(
            model="gpt-3.5-turbo",
            messages=[
                {"role": "system", "content": "You are a helpful ESL assistant."},
                {"role": "user", "content": prompt},
            ],
        )
 
def test_single_word_translation():
    # Define the input and expected output
    word = 'Bonjour'
    document_context = ''
    target_language = 'en'
    expected_output = 'Hello.'
 
    # Call the function and verify the output
    result = single_translate_word(word, document_context, target_language)
    # result = expected_output
    assert result == expected_output
 
def test_multiple_translations():
    word = "run"
    document = "She decided to go for a run."
    target_language = "Spanish"
 
    translations = multiple_translate_word(word, document, target_language)
    assert isinstance(translations, str)
    assert len(translations) > 0
 
    # Add more specific tests depending on the expected translations.
    # For example, you might check if the word "correr" is in the translations.
    assert "correr" in translations.lower()
 
def test_no_translation_handling_single():
    word = "untranslatableword"
    document = "This is a sentence with an untranslatableword."
    target_language = "Spanish"
 
    single_translation = single_translate_word(word, document, target_language)
 
    assert "unfortunately" in single_translation.lower() or "impossible" in single_translation.lower() or "unable" in single_translation.lower() or "sorry" in single_translation.lower(), "Test failed: single_translate_word() should handle untranslatable words."
 
def test_no_translation_handling_multiple():
    word = "untranslatableword"
    document = "This is a sentence with an untranslatableword."
    target_language = "Spanish"
 
    multiple_translation = multiple_translate_word(word, document, target_language)
 
    assert "unfortunately" in multiple_translation.lower() or "impossible" in multiple_translation.lower() or "unable" in multiple_translation.lower() or "sorry" in multiple_translation.lower(), "Test failed: multiple_translate_word() should handle untranslatable words."
 
def test_no_translation_handling_contextual():
    word = "untranslatableword"
    document = "This is a sentence with an untranslatableword."
    target_language = "Spanish"
 
    contextual_translation = translate_word(word, document, target_language)
 
    assert "unfortunately" in contextual_translation.lower() or "impossible" in contextual_translation.lower() or "unable" in contextual_translation.lower() or "sorry" in contextual_translation.lower(), "Test failed: translate_word() should handle untranslatable words."
 
def test_handle_different_languages():
        # Define test cases
    test_cases = [
        {
            "word": "run",
            "document": "I love to run in the park.",
            "target_language": "Spanish",
            "expected_result": "correr"
        },
        {
            "word": "book",
            "document": "I'm reading a fascinating book.",
            "target_language": "French",
            "expected_result": "livre"
        },
        {
            "word": "house",
            "document": "My house is white.",
            "target_language": "German",
            "expected_result": "Haus"
        }
    ]
 
    # Test function for each test case
    for case in test_cases:
        word = case["word"]
        document = case["document"]
        target_language = case["target_language"]
        expected_result = case["expected_result"]
 
        # Call the translate_word function
        result = translate_word(word, document, target_language)
 
        # Check if the expected result is in the translation returned
        assert expected_result.lower() in result.lower(), f"Expected '{expected_result}' in the translation, but got '{result}'"
 
def test_error_handling_on_api_failure():
    # Mock the call_chat_gpt function to raise an OpenAIError when called
    mock_call_chat_gpt = MagicMock(side_effect=OpenAIError("API call failed"))
 
    # Define the word, document, and target_language for the test
    word = "apple"
    document = "I ate an apple for breakfast."
    target_language = "French"
 
    # Use the patch context manager to replace the call_chat_gpt function with the mock
    with patch('ESL_Master.retrieve_translations_based_on_document.src.translation.call_chat_gpt', mock_call_chat_gpt):
        # Call the translate_word function and assert that it returns the expected error message
        result = translate_word(word, document, target_language)
        assert result == "An error occurred while trying to translate the word. Please try again later."
 
def test_invalid_target_language_single_translate():
    word = "hello"
    document = ""
    target_language = "invalid_language"
    expected_output = "Invalid target language."

    with patch('ESL_Master.retrieve_translations_based_on_document.src.translation.call_chat_gpt') as mock_call_chat_gpt:
        mock_call_chat_return_value = expected_output

        result = single_translate_word(word, document, target_language)
        assert result == expected_output

def test_invalid_target_language_multiple_translate():
    word = "hello"
    document = ""
    target_language = "invalid_language"
    expected_output = "Invalid target language."

    with patch('ESL_Master.retrieve_translations_based_on_document.src.translation.call_chat_gpt') as mock_call_chat_gpt:
        mock_call_chat_return_value = expected_output

        result = multiple_translate_word(word, document, target_language)
        assert result == expected_output

def test_invalid_target_language_translate_word():
    word = "hello"
    document = "Hello, how are you?"
    target_language = "invalid_language"
    expected_output = "Invalid target language."

    with patch('ESL_Master.retrieve_translations_based_on_document.src.translation.call_chat_gpt') as mock_call_chat_gpt:
        mock_call_chat_return_value = expected_output

        result = translate_word(word, document, target_language)
        assert result == expected_output

def test_translate_word_exception():
    word = "hello"
    document = "Hello, how are you?"
    target_language = "English"
    expected_output = "An error occurred while trying to translate the word. Please try again later."

    with patch('ESL_Master.retrieve_translations_based_on_document.src.translation.call_chat_gpt') as mock_call_chat_gpt:
        mock_call_chat_side_effect = OpenAIError("An error occurred")

        result = translate_word(word, document, target_language)
        assert result == expected_output






 