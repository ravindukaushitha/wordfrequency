import unittest
import sys
from pathlib import Path
sys.path.insert(0, str(Path(__file__).parent.parent.parent))
from ESL_Master.generate_contextual_examples.src.updated_chat_gpt_api import input_string_concatenation, calling_openai_api
from ESL_Master.generate_contextual_examples.src.sorting_examples_based_on_context import sort_sentences_by_similarity
from ESL_Master.generate_contextual_examples.src.sorting_based_on_vocab import sort_based_on_vocab
from ESL_Master.generate_contextual_examples.src.printing_priority_results import print_sorted_lists


vocab = ["apple", "ball", "cat", "dog", "elephant", "flower", "guitar", "hat",
         "ice cream", "juice", "key", "lion", "moon", "nurse", "orange", "pizza", "queen",
         "rainbow", "sun", "tiger", "umbrella", "violin", "whale", "xylophone", "yellow",
         "zebra", "rose", "lotus", "red", "color", "apple", "mango", "pineapple"]


class IntegrationTesting(unittest.TestCase):

    def test_integration(self):
        user_input_prompt = "animals from jungle"

        gpt_prompt = input_string_concatenation(user_input_prompt)
        gpt_output = calling_openai_api(gpt_prompt)
        sorted_similar = sort_sentences_by_similarity(gpt_output, user_input_prompt)
        sorted_vocab = sort_based_on_vocab(vocab, gpt_output)
        output = print_sorted_lists(sorted_similar, sorted_vocab)

        if len(output) > 1 and any(i.isalpha() for i in output):
            response = 200
        else:
            response = 400
        assert len(output) > 1 and any(i.isalpha() for i in output)
        print(response)

    def test_integration1(self):
        user_input_prompt1 = "i like flowers"

        gpt_prompt1 = input_string_concatenation(user_input_prompt1)
        gpt_output1 = calling_openai_api(gpt_prompt1)
        sorted_similar1 = sort_sentences_by_similarity(gpt_output1, user_input_prompt1)
        sorted_vocab1 = sort_based_on_vocab(vocab, gpt_output1)
        output1 = print_sorted_lists(sorted_similar1, sorted_vocab1)

        if len(output1) > 1 & any(i.isalpha() for i in output1):
            response1 = 200
        else:
            response1 = 400
        assert len(output1) > 1 and any(i.isalpha() for i in output1)
        print(response1)

    def test_integration2(self):
        user_input_prompt2 = "fruits"

        gpt_prompt2 = input_string_concatenation(user_input_prompt2)
        gpt_output2 = calling_openai_api(gpt_prompt2)
        sorted_similar2 = sort_sentences_by_similarity(gpt_output2, user_input_prompt2)
        sorted_vocab2 = sort_based_on_vocab(vocab, gpt_output2)
        output2 = print_sorted_lists(sorted_similar2, sorted_vocab2)

        if len(output2) > 1 and any(i.isalpha() for i in output2):
            response2 = 200
        else:
            response2 = 400
        assert len(output2) > 1 and any(i.isalpha() for i in output2)
        print(response2)
